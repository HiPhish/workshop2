(in-package #:workshop.monitor)

(defun install-monitor (path callback)
  "Installs a handler CALLBACK for changes in the PATH directory.  Any time a
change is made to PATH the CALLBACK will be called.  The Callback accepts two
arguments: the inotify EVENT and the OUT-STREAM which the `*STANDARD-OUTPUT*'
was bound to at the time this function got called.

The OUT-STREAM can be used to print messages to the `*STANDARD-OUTPUT*' of the
main thread.

FIXME: This is not recursive, it will only watch the given PATH.  This function
needs to automatically call itself when a new sub-directory is created."
  (declare (type pathname path)
           (type (function (cl-inotify::inotify-event stream) nil) callback))
  (let ((toplevel *STANDARD-OUTPUT*))
    (bt:make-thread
      (lambda ()
        (cl-inotify:with-inotify (inotify t (path '(:modify :delete :create)))
          (cl-inotify:do-events (event inotify :blocking-p t)
            (funcall callback event toplevel))))))nil)
