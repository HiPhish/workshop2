;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop)

(defparameter *main-template*
  (hssg:chain-templates #'page #'base-page))

(defparameter *website-name* "HiPhish's Workshop"
  "Name of the entire website, can be used as a fallback value when there is no
  title")

(defparameter *website-url* "https://hiphish.github.io"
  "URL of the published website. This should be overrideable somehow during the
  build process so it can be set to 'localhost'.")

(defparameter *sub-sites*
  '((:grid-framework . ((:title . "Grid Framework")
                        (:url   . ("" "grid-framework"))
                        (:items . (((:title . "Features")
                                    (:url   . ("" "grid-framework" "features"))
                                    (:tag   . :features))
                                   ((:title . "Examples")
                                    (:url   . ("" "grid-framework" "examples"))
                                    (:tag   . :examples))
                                   ((:title . "Gallery")
                                    (:url   . ("" "grid-framework" "gallery"))
                                    (:tag   . :gallery))
                                   ((:title . "Showcase")
                                    (:url   . ("" "grid-framework" "showcase"))
                                    (:tag   . :showcase))
                                   ((:title . "FAQ")
                                    (:url   . ("" "grid-framework" "faq"))
                                    (:tag   . :faq))
                                   ((:title . "News")
                                    (:url   . ("" "grid-framework" "news"))
                                    (:tag   . :news))
                                   ((:title . "Support")
                                    (:url   . "http://forum.unity3d.com/threads/grid-framework-scripting-and-editor-plugins.144886/"))
                                   ((:title . ("Buy " ((:span :class "badge") "35$")))
                                    (:url   . "https://www.assetstore.unity3d.com/#/content/62498/")))))))
  "Subsites of the website. This is a workshop-specific feature, not part of
  HSSG.")

(defparameter *footer*
  `((:logo . ((:title . "HiPhish's Workshop")
              (:image . "/img/footer/logo.png")
              (:url   . "/")))
    (:copyright . ((:note . (,(format nil "© 2015-~A, licensed under " "2022")
                             ((:a :href "http://creativecommons.org/licenses/by-sa/4.0/")
                              "CC BY-SA 4.0")))
                   (:title . "Creative Commons Attribution-ShareAlike 4.0 International License")
                   (:image . "/img/footer/cc.svg")
                   (:url   . "http://creativecommons.org/licenses/by-sa/4.0/")))
    (:social . (((:title . "GitHub")
                 (:image . "/img/footer/github.png")
                 (:url   . "https://github.com/HiPhish"))
                ((:title . "GitLab")
                 (:image . "/img/footer/gitlab.png")
                 (:url   . "https://gitlab.com/HiPhish")))))
  "Global footer of the website, can be overridden locally for individual pages.")

(defparameter *menu-bar*
  `((:left  . (((:title . "Grid Framework"  )
                (:url   . ("" "grid-framework"))
                (:items . (((:title . "Overview")
                            (:url   . ("" "grid-framework")))
                           ()
                           ((:title . "Features")
                            (:url   . ("" "grid-framework" "features")))
                           ((:title . "Examples")
                            (:url   . ("" "grid-framework" "examples")))
                           ((:title . "Gallery")
                            (:url   . ("" "grid-framework" "gallery")))
                           ((:title . "Showcase")
                            (:url   . ("" "grid-framework" "showcase")))
                           ((:title . "FAQ")
                            (:url   . ("" "grid-framework" "faq")))
                           ((:title . "News")
                            (:url   . (" " "grid-framework" "news")))
                           ()
                           ((:title . "Support")
                            (:url   . "http://forum.unity3d.com/threads/grid-framework-scripting-and-editor-plugins.144886/"))
                           ()
                           ((:title . ("Buy Now " ((:span :class "badge") "35$")))
                            (:url   . "https://www.assetstore.unity3d.com/#/content/62498")))))
              ((:title . "Open Source") 
               (:url   . "/#products")
               (:items . (((:title . "NTFS-Clone")
                           (:url   . "https://gitlab.com/HiPhish/ntfs-clone"))
                          ((:title . "IPS-Tools")
                           (:url   . "https://gitlab.com/HiPhish/IPS-Tools"))
                          ((:title . "roll")
                           (:url   . "https://gitlab.com/HiPhish/roll"))
                          ((:title . "Newton's Method in C")
                           (:url   . "https://github.com/HiPhish/Newton-method"))
                          ((:title . "Xeen Tools")
                           (:url   . "https://github.com/HiPhish/XeenTools"))
                          ((:title . "Wolf3D Extract")
                           (:url   . "https://github.com/HiPhish/Wolf3DExtract"))
                          ((:title . "Game Source Documentation")
                           (:url   . "https://github.com/HiPhish/Game-Source-Documentation")))))
              ((:title . "Vim/Nvim plugins" )
               (:url   . ("" "vim" "plugins"))
               (:items . (((:title . "Info.vim")
                           (:url   . "https://gitlab.com/HiPhish/info.vim"))
                          ((:title . "REPL.nvim")
                           (:url   . "https://gitlab.com/HiPhish/repl.nvim"))
                          ((:title . "Quicklisp.nvim")
                           (:url   . "https://gitlab.com/HiPhish/quicklisp.nvim"))
                          ((:title . "jinja.vim")
                           (:url   . "https://gitlab.com/HiPhish/jinja.vim"))
                          ((:title . "Guix channel")
                           (:url   . "https://gitlab.com/HiPhish/neovim-guix-channel/")))))))
    (:right . (((:title . "Blog"   )
                (:url   . ( "" "blog") ))
               ((:title . "About"  )
                (:url   . ("" "about"))))))
  "Global menu bar for the entire website")

(defparameter *content-tree*
  `(:module ""
    :components ((:html "index.html.lisp" "index.html")
                 (:verbatim "index.css")
                 (:directory "css")
                 (:directory "fonts")
                 (:directory "js")
                 (:module "blog"
                  :components ((:blog "HiPhish's Workshop: Blog"
                                :description "Software projects, various thoughts and ramblings"
                                :top "blog"
                                :url ("blog"))))
                 (:module "images"
                  :components ((:html "index.html.lisp" "index.html")
                               (:verbatim "analogue-data-disc-117729.jpg")
                               (:verbatim "black-black-and-white-cubes-37534.jpg")
                               (:verbatim "game-docs.png")
                               (:verbatim "grid-framework.png")
                               (:verbatim "info.vim.png")
                               (:verbatim "ips.svg")
                               (:verbatim "newton.png")
                               (:verbatim "repl.nvim.png")
                               (:verbatim "roll.jpg")
                               (:verbatim "xeen.png")))
                 (:directory "img")
                 (:module "about"
                  :components ((:html "index.html.lisp" "index.html")
                               (:module "javascript"
                                :components ((:html "index.html.lisp" "index.html")))))
                 (:module "vim"
                  :components ((:html "index.html.lisp" "index.html")
                               (:module "plugins"
                                :components ((:verbatim "extra.css")
                                             (:html "index.html.lisp" "index.html")))))
                 (:module "grid-framework"
                  :components ((:verbatim "grid-framework.css")
                               (:html "index.html.lisp" "index.html")
                               (:directory "images")
                               (:module "examples"
                                :components ((:html "index.html.lisp" "index.html")
                                             (:files "examples.css" "example.js" "web-player.css")
                                             (:grid-framework-example "dial")
                                             (:grid-framework-example "endless")
                                             (:grid-framework-example "level-design")
                                             (:grid-framework-example "lights")
                                             (:grid-framework-example "movement")
                                             (:grid-framework-example "sliding")
                                             (:grid-framework-example "snake")
                                             (:grid-framework-example "snapping")
                                             (:grid-framework-example "terrain")
                                             (:grid-framework-example "vectrosity")
                                             ))
                               (:module "faq"
                                :components ((:html "index.html.lisp" "index.html")
                                             (:verbatim "faq.css")))
                               (:module "features"
                                :components ((:html "index.html.lisp" "index.html")
                                             (:verbatim "features.css")
                                             (:verbatim "features.js")))
                               (:module "showcase"
                                :components ((:html "index.html.lisp" "index.html")
                                             (:verbatim "showcase.css")
                                             (:verbatim "gallery.js")
                                             (:directory "img")))
                               (:module "gallery"
                                :components ((:html "index.html.lisp" "index.html")
                                             (:verbatim "gallery.js")
                                             (:directory "img")))
                               (:module "news"
                                :components (
                                             (:blog "Grid Framework News"
                                              :description "News for the Grid Framework plugin for Unity3D"
                                              :top "news"
                                              :url ("grid-framework" "news")
                                              :data ((:title . "News") (:sub-site . (:grid-framework :news))))))))))
  "Specification of the website content; this tree is traversed to build up the
  artifacts of the website.")
