;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:workshop.util
  (:use :cl)
  (:export to-url-string))

(defpackage #:workshop.template.html-page
  (:use #:cl #:hssg)
  (:export page base-page grid-framework-example))

(defpackage #:workshop.artifact
  (:documentation "Artifacts specific to the workshop")
  (:use #:cl #:hssg
        #:workshop.template.html-page)
  (:import-from #:hssg
    #:derive-artifact #:compound-instruction)
  (:export grid-framework-example-artifact))

(defpackage #:workshop.server
  (:documentation "Web server suitable for use during development")
  (:use #:cl)
  (:export #:start-server #:stop-server))

(defpackage #:workshop.monitor
  (:documentation "Monitor the content directory for changes")
  (:use #:cl)
  (:export #:install-monitor))

(defpackage #:workshop
  (:use #:cl #:hssg #:workshop.artifact)
  (:import-from #:workshop.template.html-page page base-page grid-framework-example))
