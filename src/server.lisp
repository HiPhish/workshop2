;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022-2023  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop.server)

(defvar *server-port* 8080)
(defvar *server*
  (make-instance 'hunchentoot:easy-acceptor :port *server-port* :document-root #p"dist/")
  "Hunchentoot acceptor instance.")

(defun handle-directory-request ()
  "Hunchentoot request handler which returns the contents of the 'index.html'
  file for a request that ends in a slash."
  (hunchentoot:handle-static-file
    (fad:merge-pathnames-as-file
      (fad:pathname-as-directory "dist")
      (fad:pathname-as-directory (subseq (hunchentoot:request-uri hunchentoot:*request*) 1))
      (fad:pathname-as-file "index.html"))))

;;; Maybe this needs to be called after the server has been started?
(push (hunchentoot:create-regex-dispatcher "^/.+/$" 'handle-directory-request)
      hunchentoot:*dispatch-table* )

(defun start-server ()
  "Start the local web server."
  (hunchentoot:start *server*)
  (format t "Started server at http://localhost:~A" (hunchentoot:acceptor-port *server*)))

(defun stop-server ()
  "Stop the local web server."
  (hunchentoot:stop *SERVER*))
