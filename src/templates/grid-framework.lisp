;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop.template.html-page)

(deftemplate grid-framework-example (name id description content)
  (:title    (concatenate 'string name " - Grid Framework"))
  (:sub-site '(:grid-framework :examples))
  (:css      '("../web-player.css"))
  (:js       '("/js/unity-webgl.js"))
  ;; Assets built by Unity for the HTML5 player
  (:content
    `(((:link :href "example.datagz"))
      ((:link :href "example.jsgz"))
      ((:link :href "example.asm.jsgz"))
      ((:link :href "example.memgz"))
      ((:p :class "backlink")
         ((:a :href ,(concatenate 'string "../#" id))
           "Grid Framework examples")
           " | "
           (:strong ,name))
      ,description
      ((:canvas :id "canvas"
                :oncontextmenu "event.preventDefault()"
                :height "450"
                :width "600")
        "")
      ,@content
      ((:script :src "../example.js" :type "text/javascript")
        "")
      ((:script :src "UnityLoader.js")
        ""))))
