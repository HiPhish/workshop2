;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop.template.html-page)

(defun sub-site-navigation (sub-site current-tag)
  (let-metadata ((title :title)
                 (url   :url  )
                 (items :items))
      sub-site
    `((:nav :class "local-nav")
      (:ul
       ((:li ,@(if (not current-tag) '(:aria-current "page") '()))
        ((:a :href ,(workshop.util:to-url-string url))
         ,title))
       ,@(mapcar (lambda (item)
                   (let-metadata ((title :title)
                                  (url   :url  )
                                  (tag   :tag  ))
                       item
                     `((:li ,@(if (and tag (eq current-tag tag)) '(:aria-current "page") '()))
                       ((:a :href ,(workshop.util:to-url-string url))
                        ,@(if (listp title) title (list title))))))
                 items)))))

(deftemplate page (title sub-site sub-sites modified content)
  "Base template for all pages, to be spliced into a web page.

  A page is a generic template for all static pages, and one step below the
  base template. Technically the page- and base template could have been
  merged into one."
  (:content
    `(,(let ((current-sub-site (first  sub-site))
             (current-sub-tag  (second sub-site)))
         (if sub-site
           (funcall
             #'sub-site-navigation (cdr (assoc current-sub-site sub-sites)) current-sub-tag)
           '()))
      ,@content
      ,(if modified
         `(:footer
            (:p "Last updated: " (date->string modified "~1")))
         "")))
  (:title
    (if (and sub-site title)
      (format nil "~A - ~A"
              title (cdr (assoc :title (cdr (assoc (first sub-site) sub-sites)))))
      title)))
