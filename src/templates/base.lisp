;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop.template.html-page)

(defun head-snippet (&key (title nil) (css nil) (style nil) (js nil))
  "Produce the HEAD part of the base page. Returns one SXML expression (which
   may of course contain sub-expressions), not a list of SXML expressions."
  `(:head
     ((:meta :charset "utf-8"))
     ((:meta :name "viewport"
             :content "width=device-width, initial-scale=1"))
     (:title ,(or title "HiPhish's Workshop"))
     ;; My own style sheets
     ((:link :rel "stylesheet"
             :href "/css/main.css"
             :type "text/css"
             :media "all"))
     ((:link :rel "stylesheet"
             :href "/css/local-nav.css"
             :type "text/css"
             :media "all"))
     ;; Extra CSS from metadata
     ,@(mapcar (lambda (url)
		            `((:link :rel "stylesheet"
		                    :href ,url
		                    :type "text/css"
		                    :media "all")))
              css)
     ;; Extra style information embedded into the page
     ,@(mapcar (lambda (style) `(:style ,style))
              style)
     ;; Extra Javascript from metadata
     ,@(mapcar (lambda (url) `((:script :src ,url) ""))
               js)))

(defun main-navbar (home menu)
  "Generate the SXML tree of the main menu navigation bar"
  (labels ((menu-item->sxml (item &key (push-end? nil))
             (let-metadata ((title :title)
                            (url   :url  )
                            (items :items))
                 item
               ;; If the :LI is empty it is a separator, so it needs to be hidden
               `((:li ,@(if (or url items) '() '(:hidden "hidden"))
                      ,@(if push-end? '(:class "push-end") '()))
                  ,(if url
                     `((:a :href ,(workshop.util:to-url-string url))
                         ,@(if (atom title) (list title) title))
                     title)
                  ,(if items
                     `(:ul
                        ,@(mapcar #'menu-item->sxml items))
                     '())))))
    (let-metadata ((left  :left)
                   (right :right))
        menu
      `((:nav :id "main-navbar")
        ;; input and label work together for the hamburger hack
        ((:input :type "checkbox"
          :id   "main-nav-hamburger"
          :hidden "hidden"))
        (:div  ; Contains the header of the navbar
         ((:a :href "/")
          ,home)
         ((:label :for "main-nav-hamburger"
           :hidden "hidden")
          ""))
        (:ul
         ,@(mapcar #'menu-item->sxml left)
         ,(menu-item->sxml (car right) :push-end? t)
         ,@(mapcar #'menu-item->sxml (cdr right)))))))

(deftemplate base-page (site-name title menu-bar css js style footer lang content)
  "Build a complete page, ready for rendering as HTML. The content is a list of
  SXML expressions which will be spliced in. The metadata is an association
  list."
  (:content
    `((:html :lang ,lang)
       ,(head-snippet :title title :css css :style style :js js)
       (:body
         (:header
           ;; Top navigation bar
           ,(main-navbar site-name menu-bar))
         (:div
           ;; -- insert sub-navigation here ---
           ,@content)
         ;; Footer of the website
         (:footer
           (:div  ; This is a wrapper to limit the width
             ((:div :class "footer-self")
               ,(let-metadata ((logo :logo)) footer
                  (if (not logo)
                    ""
                    (let-metadata ((title :title)
                                   (image :image)
                                   (url   :url  ))
                        logo
                      `((:a :href ,url :title ,title)
                         ((:img :src ,image
                               :title ,title
                               :height "55"))))))
               ,(let-metadata ((copyright :copyright)) footer
                  (if (not copyright)
                    ""
                    (let-metadata ((note  :note)
                                   (title :title)
                                   (image :image)
                                   (url   :url))
                        copyright
                      `(:p
                         ,(if image
                            `((:a :href ,url)
                               ((:img :class "copyright-image"
                                     :src ,image
                                     :alt ,title))
                               " ")
                            "")
                         ,@note)))))
             ((:div :class "footer-social")
               ,@(mapcar (lambda (item)
                           (let-metadata ((url   :url  )
                                          (title :title)
                                          (image :image))
                               item
                             `((:a :href ,url :title ,title :target "blank")
                                ((:img :src    ,image :alt    ,title :height "55"))
                                " ")))
                         (cdr (assoc :social footer))))))))))
