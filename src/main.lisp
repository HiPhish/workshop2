;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022-2023  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG.  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop)

(defparameter *base-data*
  `((:site-name . ,*WEBSITE-NAME*)
    (:title     . ,*WEBSITE-NAME*)
    (:url       . ,*WEBSITE-URL*)  ; The URL should be overridden by CLI arguments
    (:sub-sites . ,*SUB-SITES*)
    (:menu-bar  . ,*MENU-BAR*)
    (:footer    . ,*FOOTER*)
    (:lang      . "en"))
  "Metadata to be passed to the base page template")

(defvar *output* (make-instance 'hssg:base-file-system :directory #p"dist")
  "The main output file system")

(defvar *content* (fad:pathname-as-directory #p"content")
  "The root directory where the content is saved.")

(defun build-website (content-tree)
  (trivia:ematch content-tree
    ((list :html lisp html)
     (let ((artifact (hssg:read-html-lisp
                       (fad:merge-pathnames-as-file *content* lisp)
                       html
                       :template *main-template*
                       :initial *base-data*)))
       (hssg:write-to-filesystem (derive-artifact artifact) *output*)))
    ((list :module module :components components)
     (let ((*content* (fad:merge-pathnames-as-directory *content* (fad:pathname-as-directory module)))
           (*output*  (make-instance 'hssg:overlay-file-system
                        :parent *output* :directory (fad:pathname-as-directory module))))
       (dolist (component components)
         (build-website component))))
    ((list :verbatim path)
     (let ((artifact (hssg:make-verbatim-artifact *content* (fad:pathname-as-file path))))
       (hssg:write-to-filesystem (derive-artifact artifact) *output*)))
    ((list* :files files)
     (let ((artifact (apply #'hssg:make-compound-artifact (mapcar (lambda (file)
                                                                    (hssg:make-verbatim-artifact
                                                                      *content*
                                                                      (fad:pathname-as-file file)))
                                                                  files))))
       (hssg:write-to-filesystem (derive-artifact artifact) *output*)))
    ((list :directory path)
     (let ((artifact (hssg:make-directory-artifact *content* (fad:pathname-as-directory path))))
       (hssg:write-to-filesystem (derive-artifact artifact) *output*)))
    ((list :grid-framework-example name)
     (let ((artifact (make-instance 'grid-framework-example-artifact
                       :name name :path *content* :initial *base-data*)))
       (hssg:write-to-filesystem (derive-artifact artifact) *output*)))
    ((trivia:plist :blog title :description description :top top :url url :data data)
     (let ((blog (hssg.blog:make-blog
                   *content* :title title :url url :description description :top top
                   :template (hssg:template-with-data
                               *MAIN-TEMPLATE* (concatenate 'list data *BASE-DATA*)))))
       (hssg:write-to-filesystem (derive-artifact blog) *output*)))))

(defun build ()
  "Builds the website"
  (let ((*PRINT-PRETTY* nil)
        (hssg:*site-url* "https://hiphish.github.io")
        (*main-template* (hssg:chain-templates #'page #'base-page)))
    (build-website *CONTENT-TREE*)))

(workshop.monitor:install-monitor
  *content*
  (lambda (event out-stream)
    (let ((fname (cl-inotify:inotify-event-name event)))
      (format out-stream "File ~A modified, rebuilding website" fname)
      (build))))


(build)
(workshop.server:start-server)
