;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:workshop.artifact)

(defconstant +static-assets+
  (list "example.asm.jsgz" "example.datagz" "example.jsgz" "example.memgz" "UnityLoader.js")
  "These file names are fixed for all examples.")

(defclass grid-framework-example-artifact ()
  ((gf-example-name :initarg :name
                    :documentation "Name of the directory where the example is stored.")
   (gf-example-path :initarg :path
                    :documentation "Path to the example, relative to content
directory, excluding the example itself")
   (gf-example-initial-data :initarg :initial))
  (:documentation "A playable example of Grid Framework

Each example is an on-disc directory with fixed contents:

  - index.html.lisp  A static page that used the GRID-FRAMEWORK-EXAMPLE template
  - example.asm.jsgz
  - example.datagz
  - example.jsgz
  - example.memgz
  - UnityLoader.js"))

(defmethod write-artifact ((artifact grid-framework-example-artifact))
  (with-slots ((name gf-example-name)
               (path gf-example-path)
               (initial gf-example-initial-data)) artifact
      (let ((index.html (read-html-lisp (format nil "content/~A/~A/index.html.lisp" path name)
                                        (format nil "output/~A/~A/index.html"       path name)
                                        :template (lambda (data)
                                                    (base-page (page (grid-framework-example data))))
                                        :initial initial))
            
           (assets (mapcar (lambda (fname)
                             (make-verbatim-artifact (format nil "./~A/~A/~A" path name fname)
                                                     #p"content"
                                                     #p"output"))
                           '("example.asm.jsgz" "example.datagz" "example.jsgz"
                             "example.memgz" "UnityLoader.js"))))
        (write-artifact
          (apply #'hssg:make-compound-artifact (cons index.html assets))))))

(defmethod derive-artifact ((artifact grid-framework-example-artifact))
  (with-slots ((name gf-example-name)
               (path gf-example-path)
               (initial gf-example-initial-data))
      artifact
    (let ((index.html (read-html-lisp (fad:merge-pathnames-as-file
                                        path
                                        (fad:pathname-as-directory name)
                                        (fad:pathname-as-file "index.html.lisp")) 
                                      (fad:merge-pathnames-as-file
                                        (fad:pathname-as-directory name)
                                        (fad:pathname-as-file "index.html"))
                                      :template (lambda (data)
                                                  (base-page (page (grid-framework-example data))))
                                      :initial initial))
          
         (assets (mapcar (lambda (fname)
                           (make-verbatim-artifact
                             path
                             (fad:merge-pathnames-as-file
                               (fad:pathname-as-directory name)
                               (fad:pathname-as-file fname))))
                         +static-assets+))
         )
      (make-instance 'compound-instruction
        :instructions (mapcar #'derive-artifact (cons index.html assets))))))
