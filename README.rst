.. default-role:: code


####################
 HiPhish's Workshop
####################

Source code of `my personal web site`_, uses `HSSG`_ as its static site generator.

Hacking
#######

There are two main directories: `src` and `content`.

The `src` directory contains all the Common Lisp code which is needed to build
the page and run a local web server, mostly custom HSSG templates and
artifacts.

The `content` directory contains the actual content of the web site, mostly as
Lisp code and Markdown.  The source code traverses the content and assembles
the artifacts which will be written to the `output` directory.


Building
########

Currently the only way to build the website is to load the `workshop` system.
This will build the site for publishing and start the local web server.  When I
get a better understanding of `ASDF`_ I want to be able to build the site for
publishing without starting the server, or for development which will also
start the server.


License
#######

The content is licensed under the `CC BY-SA 4.0`_ license, the source code
under the GNU Affero General Public License v3.0 or later (SPDX_ license
identifier `AGPL-3.0-or-later`)license.


.. _my personal web site: https://hiphish.github.io/
.. _HSSG: https://gitlab.com/HiPhish/cl-hssg
.. _ASDF: https://asdf.common-lisp.dev/
.. _CC-BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/
