(asdf:defsystem #:workshop
  :description ""
  :author "HiPhish <hiphish@posteo.de>"
  :license "AGPL-3.0-or-later AND CC-BY-SA-4.0"
  :version "0.0.0"
  :depends-on ("hssg" "hssg-blog" "hssg-blog-cmark" "trivia" "hunchentoot" "cl-inotify")
  :serial t
  :components (
               (:module "src"
                :components ((:file "package")
                             (:file "util")
                             (:module "templates"
                              :components ((:file "page")
                                           (:file "base")
                                           (:file "grid-framework")))
                             (:file "artifacts")
                             (:file "config")
                             (:file "server")
                             (:file "monitor")
                             (:file "main")))))
