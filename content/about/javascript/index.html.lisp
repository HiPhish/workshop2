(use-package '#:hssg)

(defun js-entry->sxml (name license my-url original-url license-url)
  "Build up an SXML table entry from information about a JavaScript source
file."
  `(:tr
     (:td
       (:a :href ,my-url
         ,name))
     (:td
       (:a :href ,license-url
         ,license))
     (:td
       (:a :href ,original-url
         ,name))))

;; Use this information to build up the table for Javascript files
(defvar *javascript*
  ;; name license my-url original-url license-url
  '(("jquery.min.js"
     "X11"
     "/js/jquery.min.js"
     "https://github.com/jquery/jquery"
     "http://www.xfree86.org/3.3.6/COPYRIGHT2.html#3")
    ))

(static-page ((title :title "HiPhish's Workshop - Javascript"))
  '(:p
     "The following is a list of all JavaScript scripts employed on this page.")
  `(:table :class "table"
           :id    "jslicense-labels1"
    ,@(mapcar (lambda (js-entry) (apply #'js-entry->sxml js-entry))
              *javascript*)))
