(use-package '#:hssg)

(static-page ((title :title "HiPhish's Workshop - About"))
  '(:h1 "About me")
  '(:h2 "About the author")
  '(:p
     "Hi, I'm HiPhish and this is my website. I'm a mathematician and a
     programmer and my motto is "
     (:em "I want it to to exist, so I'll make it")
     ". The goal is rock-solid software that is reliable and fit for
     productive use, because I wouldn't want to use anything else myself.")
  '(:p
     "I chose the "
     (:em "Workshop")
     " motto because I consider programming to be a craft. This site is where
     I exhibit my work, present my services, and if you follow the links to my
     repositories you can see the details or how it is made. Programming is a
     new form of craftsmanship and the computer is our workbench.")
  '(:h3 "Tips and sponsorship")
  '(:p
    "If you like my work you can leave a tip or sponsor me.")
  '(:ul
    (:li "Monero donation wallet: "
         (:code "42MKtvm2XkS6pSKx2Dd4P9Wfty2pefHbfjU2y2YoG4AKeEaNzRwnDbNX4hzAWeDpR8ZeecutY9Tmv51GLp7Ltgaf7ReHpFT"))
    (:li ((:a :href "https://github.com/sponsors/HiPhish") "GitHub sponsorship")))
  '(:hr)
  '(:h2 "About this website")
  '(:p
    "The Workshop would not be in its current form if it were not for these
     great tools written by other people.")
  '(:dl
    (:dt
     ((:a :href "https://gitlab.com/HiPhish/cl-hssg") "CL-HSSG"))
    (:dd
     (:p
       "The Hackable Static Site Generator, my own static site generator which
       can be fully scripted in Common Lisp. With the extensibility of Lisp it
       is possible to use a real programming language to mold the generator for
       any kind of website."))
    (:dt
      ((:a :href "http://www.no-margin-for-errors.com/projects/prettyPhoto-jquery-lightbox-clone/")
        "prettyPhoto"))
    (:dd
      (:p
        "A JavaScript gallery plugin that provides nice looking overlays
        for images. You can also navigate between images without leaving
        the gallery."))
    (:dt
      ((:a :href "http://philipwalton.github.io/solved-by-flexbox/")
        "Solved by Flexbox"))
    (:dd
      (:p
        "Flexbox is a new CSS feature that allows you to lay out content
        in a flexible way. Instead of hardcoding values position, size or
        order of elements can be given relative to each over other or the
        enclosing container.  Flexbox makes many previously hard and hacky
        problems trivial to solve. "
        (:em "Solved by Flexbox")
        " features a collection of solutions to common problems.  In my case
        I used it for the sticky footer and the blog layout.")))
  '(:p
    "All content is written in Neovim and built using "
    ((:a :href "https://sbcl.org/") "SBCL")
    " as the Lisp implementation. The website content is version-controlled
    using Git.")
  '(:hr)
  '(:h2 "LibreJS - Free JavaScript")
  '(:p
    "The Workshop is build from free (libre) software that respects the
    user's freedom. The same also applies to the JavaScript employed on this
    site. I have made sure that the site complies with the LibreJS
    specifications. You can find a list of scripts employed under "
    ((:a :href "javascript")
      "about/javascript/")
    ".")
  '(:hr)
  '(:h2 "Source code")
  '(:p
    "The Scheme source code for the workshop is available in a "
    ((:a :href "https://github.com/HiPhish/workshop")
      "public repo")
    ". If anything on the website is not working, please file an issue.")
  '((:h2 :id "content-license")
     "License")
  '(:p
    "Unless otherwise noted the content on this site is licensed under the
    Creative Commons "
    ((:a :href "http://creativecommons.org/licenses/by-sa/4.0/")
      "Attribution-ShareAlike 4.0 International")
    " license (CC BY-SA 4.0). In short, the license allows you to use content
    even for commercial use, as long as you make sure to to give proper
    credit.  Please read the license for detailed information."))
