(use-package '#:hssg)

(defun gallery-item->sxml (item)
  (destructuring-bind (img caption alt) item
    `(:li
       ((:a :title ,caption
            :href  ,(concatenate 'string "img/" img)
            :class "thumbnail")
         ((:img :src   ,(concatenate 'string "img/" img)
                :class "group1"
                :alt   ,alt))))))

(defvar *gallery-items*
  ;; Image URL, caption, alternative text
  '(("grids.png"
     "Adding a grid to the scene is as easy as assigning any other component in Unity."
     "Four kinds of grids: rectangular, spherical, polar and hexagonal")
    ("renderers.png"
     "Renderers display the grid in the scene; use one of the existing ones or write your own."
     "Customizable grid renderers")
    ("documentation.png"
     "The user manual explains the components and how to use them, the scripting reference covers every part of the API."
     "User manual and reference manual included")
    ("menu-bar.png"
     "No menu clutter, everything fits inside Unity as it should."
     "Menu items are integrated into Unity's menus")
    ("lights.png"
     "The included example shows how a grid can be used as a common base of communication between unrelated objects."
     "Example included: lights-out game")
    ("movement.png"
     "Use grids to compute way-points for grid-based movement."
     "Example included: movement and snapping")
    ("infinite.png"
     "Adjust the range of grid renderer on the fly to create the illusion of an infinitely scrolling grid."
     "Example included: infinitely scrolling grid")
    ("terrain.png"
     "Generate a mesh from simple data by converting the data to world-coordinates."
     "Example included: generate mesh from data")
    ("level.png"
     "Place objects in the world based on grid coordinates."
     "Example included: build levels from data")
    ("dial.png"
     "Use the grid coordinates of a point to drive an animation script"
     "Example included: rotary dial")
    ("vectrosity.png"
     "Users of Vectrosity can have the lines of renderer generate with one method call"
     "Vectrosity support")
    ("playmaker.png"
     "Playmaker bindings for the Grid Framework API enable visual scripting"
     "Playmaker support")))

(static-page ((title    :title    "Gallery")
              (sub-site :sub-site '(:grid-framework :gallery))
              (css      :css      '("/css/gallery.css")))
  '(:p
     "Click on one of the thumbnails to see the full image or mouse over for
     the description. The examples shown here are all included with Grid
     Framework.  Please keep in mind that the "
     ((:a :href "https://starscenesoftware.com/vectrosity.html")
       "Vectrosity")
     " example requires you
     to own a Vectrosity license.")
  `((:ul :id "gallery" :class "gallery thumbnails")
     ,@(mapcar #'gallery-item->sxml *gallery-items*))
  '((:div :id "gallery-modal" :class "hidden")
    "")
  '((script :type "module" :src "gallery.js" :chartset "utf-8")
    ""))
