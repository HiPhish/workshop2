(use-package '#:hssg)

(static-page ((name :name "Terrain mesh generation")
              (id   :id   "terrain")
              (description :description
                           '(:p "Left-click a vertex to raise it, right click
                                to lower it.")))
  '(:p "The text field visualises the state of the terrain matrix."))
