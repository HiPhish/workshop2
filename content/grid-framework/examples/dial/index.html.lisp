(use-package '#:hssg)

(static-page ((name        :name "Rotary Dial")
              (id          :id   "dial")
              (description :description
                           '(:p "Click a number on the dial to spin it."))))
