(use-package '#:hssg)

(static-page ((name :name "Runtime snapping")
              (id   :id   "snapping")
              (description :description
                           '(:p "Click and drag a block over the grid and
                                 observe how it snaps to the grid's cells.")))
  '(:p " The mouse input is handled by casting a ray from the cursor
			through the camera into the grid and seeing where it hits the grid's
			collider."))
