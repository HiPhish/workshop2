(use-package '#:hssg)

(static-page ((name :name "Vectrosity support")
              (id   :id   "vectrosity")
              (description :description
                           '(:p "The lines are rendered through Vectrosity."))))
