title: Homestrech
category: progress
tags: v3
---

It has taken more time than I had hoped, but Grid Framework version 3.0 is
finally almost done. All the code has been written and tested, the logistics
have been figured out, and the documentation has been updated. This has been
quite and exciting year and it is good to see the goal ahead.

So what is still missing? For one, I had a really nasty data loss recently, and
while I had everything important backed up, it is the many small and seemingly
unimportant files that slow down the recovery process. I have all the small
stuff sorted out, but the toughest nut to crack has ironically been Unity3D
itself.

There are four different types of files on my computer:

- Files I explicitly created myself
- Files that belong to the system
- Foreign files that are freely available
- Foreign files under lock and seal

The first kind is easy enough, I created them willingly, I decide if they are
worth backing up. The second kind is irrelevant, they will be replaced by the
new system anyway. The third kind can be a bit of work, but since they are
freely available it is usually just a matter of a simple download. The fourth
kind is where it's at though: you have to jump through hoops to get them, and
even when you have them you need to jump through even more hoops to get them to
actually work. I have written about it in my [main blog].

What is the solution here? Back up the proprietary files? That can be
problematic if the backup gets stolen and the files leaked. The files might
contain a unique identifier tied to you, so good luck proving that you did not
leak them yourself. There is also no guarantee that the backed up files will
work from a backup and won't want to re-authenticate, which brings us right
back to the original issue. Honestly, I have no idea how to deal with this kind
of problem, other than to just hope for the best. Or don't use proprietary
software in the first place I guess.


[main blog]: /blog/2021/10/11/getting-unity-hub-3-working-on-gnu-linux/
