title: I have a GitHub sponsor page now
category: organisation
---

I have decided to open up a [GitHub sponsor page].  Recently I released the
Neovim plugin [nvim-ts-rainbow2], which has garnered a lot of stars, so I
figured I might as well try this sponsor thing.  There is also a Monero wallet
for those who prefer to forgo the middle-man.

When I started uploading code to the internet it was mostly code written by me
for myself.  Sure, there was a good amount of documentation and commentary so
other people could profit, but let's be honest: most people don't care about
the code, they want a software they install via a default package manager, or
at least build with minimal effort.

I want to make my software more than a disposable hackjob, I want to give it
the final polish to become a complete product which can be of use to other
people.  But polishing takes time, which is hard to justify when the software
already does all I personally need from it.  Sponsorship can help me push over
that last hurdle.  What does it mean to give a software the last polish?

- Proper test coverage (unit, integration, end-to-end)
- Use of a standard or well-known build system for easy installation
- Inclusion in a default package manager
- Proper documentation

Of course not all of these points are always applicable and there might be some
more points that I have forgotten, but the point stands.


[GitHub sponsor page]: https://github.com/hiphish/sponsor
[nvim-ts-rainbow2]: https://gitlab.com/HiPhish/nvim-ts-rainbow2
