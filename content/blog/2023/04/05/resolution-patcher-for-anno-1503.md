title: Resolution patcher for Anno 1503
tags: games
---

I have written a small [Python script] which patches the game files of Anno 1503
to run at higher resolutions.  There is already a [widescreen patch] out
there ready to use, so why create a patcher?  Download links die, knowledge
gets forgotten and people lose interest and move on.  Without preservation of
knowledge the patch will be lost and impossible to reproduce without trial and
error all over again.  My patcher exists both to be useful, and as executable
documentation for posterity.  There is also the possibility that if you patch
the DLL file that came with your own game instead of using someone else's it
might run more stable (not all builds of the game are identical), but that's
just a blind guess.

Game modding has a general problem with lack of reproducibility.  Someone will
hunker down, reverse engineer code and file formats, write a program, only to
just release the binaries with no source code.  All that knowledge then remains
in his brain until he gets bored and moves on.  This patcher is my attempt to
alleviate the problem at least a tiny bit where I can.

[Python script]: https://gitlab.com/HiPhish/anno-1503-resolution-patcher
[widescreen patch]: https://www.annopool.de/filebase/index.php?file/2281-widescreen-patch/
