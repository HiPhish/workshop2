title: Free Software is important for non-programmers as well
tags: rant
category: open-source
---

[Free Software] is software which respects the user's freedom.  The Four
Freedoms ensure that users can run the program for any purpose, study the
source code, modify the source, share the software and share modifications of
the software.  However, what if you are not a programmer? Does this mean Free
Software only adds value to people who know how to program, and if you are a
non-programmer you might as well use proprietary software?  Is it all just the
same then?  No.  In this post I will try to illustrate why Free Software
matters even to non-programmers.

I am not a plumber, and for the sake of argument I will assume neither are you.
So let's say you need the plumbing in your bathroom done.  You then call a
couple of plumbing companies, compare their offers, pick one and have them
install new pipes.

Now assume the plumbing company were to install some proprietary pipes.  No
other plumber's tools work with these pipes and no regular pipes can be
connected to the proprietary pipes.  Even if someone wanted to make a pipe
adapter the first plumbing company would sue him.

But hey, you aren't a plumber, so what kind of difference does this make to
you?  It does make all the difference.  Maybe the original plumbing company no
longer exists.  Maybe you did not like their service.  Maybe the next
appointment they can schedule is in two months, but you need your bathroom now.
Maybe they have hiked up their prices to unreasonable levels.  Maybe there is a
better plumbing company you would like to hire instead.  Maybe the repair is so
simple that even you could do it.

Whatever the reason is, it does not matter.  You should not have to justify why
you do not want a company to hold your plumbing hostage.  Being free to choose
who does the plumbing in your bathroom should be the default.  And it is the
default, you do not have to rip out your entire plumbing for a repair without
reason.

Software is similar.  Even if you yourself cannot do anything with the source
code, someone else can.  You can either hope someone else happens to have the
same needs and shares his modification, or you can actually hire someone to
make those modifications.  You could hire the original developer (who knows the
software best), but you do not have to, you can hire anyone with the necessary
skills.

Free Software is essential in making sure your computing is not being held
hostage by anyone.  I would even argue that Free Software for non-programmers
is actually more import that for programmers.  As a programmer I could write my
own software; it would probably take quite a lot of time and effort to the
point where it does not really make sense, but I could do it.  As a
non-programmer though you cannot just roll your own, so you are dependent on
other people, and you want to be able to pick the best one for the job.


[Free Software]: https://www.gnu.org/philosophy/free-sw.html
