title: cl-cmark approaching stable release
category: open-source
tags: lisp, markdown
---

In June of this year I introduced my Common Lisp library [cl-cmark] ([GitHub
mirror]), which implements parsing [CommonMark] documents and manipulating the
resulting document tree. I have been hammering out the last kinks over the past
weeks and I am now ready to call it done. Before I officially put the 1.0 stamp
on it though I would like if a few more people could take a look at the library
and give it a try.

I have filed a request ([#2230]) for inclusion in [Quicklisp]. Until then you
will have to check out the library from source. If you have any suggestions or
issues please open an issue on GitLab or GitHub.




[cl-cmark]: https://gitlab.com/HiPhish/cl-cmark
[GitHub mirror]: https://github.com/hiphish/cl-cmark
[cmark]: https://github.com/commonmark/cmark
[#2230]: https://github.com/quicklisp/quicklisp-projects/issues/2230
[Quicklisp]: https://www.quicklisp.org/
