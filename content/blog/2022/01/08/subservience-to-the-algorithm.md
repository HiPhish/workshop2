title: Subservience to the algorithm
tags: rant
---

Over the last couple of years I have been noticing two trends among internet
videos, especially on YouTube: videos that could easily be three to four
minutes long are stretched to over ten minutes in length, and thumbnails where
people are making stupid faces. This comes from YouTube's recommendation
algorithm favouring thumbnails with people's faces, and videos needing to be at
least ten minutes long to be eligible for advertising revenue. At least those
used to be the rules back then, they might have changed since, but the videos
still follow the same pattern.

Have you ever watched a video thinking “This is a nice video, but you know what
would make it even better? If there was a chubby neckbeard trying to swallow
the camera in the thumbnail.”? Or how about “I really hate how concise this is,
I wish it was stretched to three times its length”? Yeah, me neither. This is
why I titled this post *subservience to the algorithm*. None of these points
actually benefit the viewer, yet the producer does it anyway. It benefits the
producer not because this is what people want to see, but because this is what
YouTube wants people to see.

I understand why video producers do it, they are dependent on the revenue and
as such they have to game the algorithm. But that is looking at the symptom,
not the cause. The cause is that when the content is free the viewer is not the
customer, the viewer is the product. YouTube started out as a video sharing
website where you could upload anything: a funny skit, an informational video,
your life story or just random nonsense. YouTube also started out as a pure
money sink because no one was paying for it. Google has effectively turned
YouTube into modern-day TV. Go ahead, turn on private browsing in your web
browser (or even better, delete your cookies and set the browser to auto-delete
cookies) and open YouTube. What do you see trending? Celebrity gossip, music,
clickbait and literal advertising. We went to YouTube because we wanted to get
away from TV, but TV has caught up with us.

Perhaps it is time to give up the pipe dream of making a living off free
internet videos. When it comes to making money off internet videos I see two
choices: either go fully in and become a slave to the algorithm, or treat is as
a hobby and if some pocket change comes rolling your way that's nice, but don't
rely on it.

The issue is not that some videos are long; I very much enjoy a long video if
it actually has enough content to justify its length. For instance, today I
watched an hour-long presentation by the Lockpicking Lawyer which he gave at
Saintcon ([YouTube link](https://www.youtube.com/watch?v=IH0GXWQDk0Q)); his
videos are usually just a few minutes long and in the talk he even addresses
why he keeps his videos this short.

How can we solve this problem? I don't have an answer, but I have some ideas:

- Direct patronage by viewers
- Sponsored content
- Using the videos to promote your actual business

The first point is what most people rely on when their content does not fit the
TV show pattern. The second point is similar, possibly even more profitable,
but it comes with more hooks attached because companies are picky about who can
represent their products. It also means that viewers can assume that your
videos are biased towards your sponsor.

The third point is perhaps the best option. By “actual business” I mean a
business that actually sells a product or provides a service. It does not need
to be big, the point is that you have someone who is willing to pay hard cash
for something with a price tag. The videos can be directly or just tangentially
related to the business, the point is not to rely on the videos *directly* for
your income.

However, in the end this brings us back to the original issue: even if you are
relying on other sources of revenue you still need to appease the algorithm in
some way in order to get your promotion actually seen. Yes, you will still be
subservient to the algorithm, but less so. You can host your videos on other
websites, or even host your own [Peertube](https://joinpeertube.org/) instance.
Freedom is not a binary state, everyone is a subservient to something, but we
still decide how much we want to be subservient to what.
