title: What even is user-friendly?
category: misc
tags: rant
----

The term “user-friendly” gets thrown around often, but what does it even mean
to be “user-friendly”? Clearly we can say that something is more user-friendly
the fewer defects it has. But beyond that? Is a GUI application more
user-friendly than a text-based application? Is a lower learning curve more
user-friendly than a steep learning curve? Is a product which has many features
built-in more user friendly than a barebones product or an extensible product?

I propose that the term is meaningless. Friendly to whom? The issue is that
there is not The One User, different people have different needs. Consider a
bicycle compared to a tricycle. Clearly the tricycle is easier to learn, even a
child can do it. So clearly tricycles are more user-friendly than bicycles. Yet
we do not see many tricycles on the streets, people who have mastered the steep
learning curve of a bicycle seem to stick to it.

Is it a sunk-cost fallacy where after spending so much effort learning to ride
a bicycle we do not want to admit that it was all for nothing? Of course not.
Once we overcome the initial difficult riding a bicycle is actually easier in
the long run. It is easier to maneuver, it takes up less space, and it is
easier get up to speed. Tricycles for most people are just an intermediate step
until we learn to keep the balance on a bicycle. Adult tricycles do exist, but
they only used for niche purposes like transporting heavy loads where the
advantages of a bicycle are negated.

When people say that something is “user-friendly” they really mean that it is
*beginner-friendly*, but they avoid the term “beginner” as if it is something
to be ashamed of. Being a beginner is not shameful, everyone starts out as a
beginner. Some people remain on the beginner level for years, and that is
perfectly fine too. There are only so many skills that one can master in a
lifetime.

I have a heavily customised Neovim setup as my text editor. I use a tiling
window manager. I use the terminal for most of my computing. I use [Void
Linux](https://voidlinux.org/) as my operating system. This web paged is
rendered using my custom written static site generator. I would not recommend
any of these things to a beginner because mastering the complexity is not worth
the effort if you are not going to get heavy use out of it. On the other hand,
I put up with the complexity not because I am some sicko who enjoys the pain,
but because with my heavy use of computers what I gain in efficiency and
control more than makes up for the initial effort.

It is a OK to be a beginner. It is OK to remain a beginner in some fields. Not
everyone can be good at everything. Please don't sugar-coat terms out of some
misplaced sense of shame. There has to be a name for the kind of person who
just brushes off everything he does not know as stupid. Like some sort of
reverse imposter syndrome.
